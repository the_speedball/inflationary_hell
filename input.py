import datetime
import os

from jinja2 import Environment, FileSystemLoader, select_autoescape

print("rendering")

vars_from_env = {
    "current_inflation_rate": os.environ["CURRENT_INFLATION_RATE"],
    "current_unemployment_rate": os.environ["CURRENT_UNEMPLOYMENT_RATE"],
    "current_misery_index": os.environ["CURRENT_MISERY_INDEX"],
    "previous_misery_index": os.environ["PREVIOUS_MISERY_INDEX"],
    "current_date": datetime.datetime.now().strftime("%Y-%m-%d"),
}

env = Environment(loader=FileSystemLoader("template"), autoescape=select_autoescape())
template = env.get_template("index.jinja")

# generate new site version
with open("public/index.html", "w") as f:
    f.write(template.render(**vars_from_env))
